#include <SoftwareSerial.h>
#include <SoftPWM.h>
#include <Servo.h> //between 0 and 180 degrees

#define LEDPIN 13

//測距センサ
#define FRONT_DIST_PIN A1
#define BACK_DIST_PIN A0
bool front_safe = false, back_safe = false;
bool front_safe_old = false, back_safe_old = false;
#define DIST_BUF_SIZE 10
int front_dist_buf[DIST_BUF_SIZE] = {};
int back_dist_buf[DIST_BUF_SIZE] = {0};
int front_dist_idx = 0, back_dist_idx = 0;

//サーボ
#define TopServoPin 9
#define SRVSTEP 5
Servo topsrv;
#define TopServoMin 90
#define TopServoMax 150
volatile int topdeg = 110;
volatile int topdeg_r = 110;
#define DegUpdateStep 2
volatile int degupdate = 0;
//pwm value
//max : 254
//init : 220
//min : 180(uemuku)

//モータ
#define MotorR_IN1 2
#define MotorR_IN2 3
#define MotorL_IN1 4
#define MotorL_IN2 5

//Motor_RUN[%]のPWM
#define Motor_RUN 25

//進行方向
#define MT_STOP 0
#define MT_FORWARD 1
#define MT_BACK 2
#define MT_RIGHT 3
#define MT_LEFT 4
volatile int mt = MT_STOP;

#define BlueTooth_TX 10
#define BlueTooth_RX 11
SoftwareSerial mySerial(BlueTooth_TX, BlueTooth_RX); //RX, TX

void setPWMFadeTime(int up, int down)
{
  SoftPWMSetFadeTime(MotorR_IN1, up, down);
  SoftPWMSetFadeTime(MotorR_IN2, up, down);
  SoftPWMSetFadeTime(MotorL_IN1, up, down);
  SoftPWMSetFadeTime(MotorL_IN2, up, down);
}

int degToPWM(int deg)
{
  return (int)(((double)deg-90.0)*1.23 + 180);
}

void control()
{
  int distFront = analogRead(FRONT_DIST_PIN);
  int distBack = analogRead(BACK_DIST_PIN);

  front_dist_buf[front_dist_idx] = distFront;
  back_dist_buf[back_dist_idx] = distBack;
  if(front_dist_idx == DIST_BUF_SIZE - 1) front_dist_idx = 0;
  else front_dist_idx++;
  if(back_dist_idx == DIST_BUF_SIZE - 1) back_dist_idx = 0;
  else back_dist_idx++;
  int frontAve = 0, backAve = 0;
  for(int i = 0; i < DIST_BUF_SIZE; i++)
  {
    frontAve += front_dist_buf[i];
    backAve += back_dist_buf[i];
  }
  frontAve /= DIST_BUF_SIZE;
  backAve /= DIST_BUF_SIZE;

/*Serial.print(distFront);
  Serial.print("\t:\t");
  Serial.println(frontAve);*/

  //測距センサ : 閾値250 大きい時は床あり、小さいときは床なし
  if(frontAve < 250) front_safe = false;
  else front_safe = true;
  if(backAve < 250) back_safe = false;
  else back_safe = true;

  if(front_safe_old && !front_safe && mt == MT_FORWARD)
  {
    //前危険になった
    mySerial.write('h');
    Serial.write('h');
  }
  if(!front_safe_old && front_safe && mt == MT_BACK)
  {
    //前安全になった
    mySerial.write('n');
    Serial.write('n');
  }
  if(back_safe_old && !back_safe && mt == MT_BACK)
  {
    //後ろ危険になった
    mySerial.write('j');
    Serial.write('j');
  }
  if(!back_safe_old && back_safe && mt == MT_FORWARD)
  {
    //後ろ安全になった
    mySerial.write('m');
    Serial.write('m');
  }

  front_safe_old = front_safe;
  back_safe_old = back_safe;

  if(mt == MT_FORWARD)
  {
    if(front_safe)
    {
      SoftPWMSetPercent(MotorR_IN1, Motor_RUN); SoftPWMSetPercent(MotorR_IN2, 0);
      SoftPWMSetPercent(MotorL_IN1, Motor_RUN); SoftPWMSetPercent(MotorL_IN2, 0);
    }
    else
    {
      mt = MT_STOP;
      setPWMFadeTime(100, 0);
      SoftPWMSetPercent(MotorR_IN1, 100); SoftPWMSetPercent(MotorR_IN2, 100);
      SoftPWMSetPercent(MotorL_IN1, 100); SoftPWMSetPercent(MotorL_IN2, 100);
      setPWMFadeTime(100, 50);
    }
  }
  else if(mt == MT_BACK)
  {
    if(back_safe)
    {
      SoftPWMSetPercent(MotorR_IN1, 0); SoftPWMSetPercent(MotorR_IN2, Motor_RUN);
      SoftPWMSetPercent(MotorL_IN1, 0); SoftPWMSetPercent(MotorL_IN2, Motor_RUN);
    }
    else
    {
      mt = MT_STOP;
      setPWMFadeTime(100, 0);
      SoftPWMSetPercent(MotorR_IN1, 100); SoftPWMSetPercent(MotorR_IN2, 100);
      SoftPWMSetPercent(MotorL_IN1, 100); SoftPWMSetPercent(MotorL_IN2, 100);
      setPWMFadeTime(100, 50);
    }
  }
  else if(mt == MT_RIGHT)
  {
    SoftPWMSetPercent(MotorR_IN1, 0); SoftPWMSetPercent(MotorR_IN2, Motor_RUN);
    SoftPWMSetPercent(MotorL_IN1, Motor_RUN); SoftPWMSetPercent(MotorL_IN2, 0);
  }
  else if(mt == MT_LEFT)
  {
    SoftPWMSetPercent(MotorR_IN1, Motor_RUN); SoftPWMSetPercent(MotorR_IN2, 0);
    SoftPWMSetPercent(MotorL_IN1, 0); SoftPWMSetPercent(MotorL_IN2, Motor_RUN);
  }
  else //if(mt == MT_STOP)
  {
    SoftPWMSetPercent(MotorR_IN1, 0); SoftPWMSetPercent(MotorR_IN2, 0);
    SoftPWMSetPercent(MotorL_IN1, 0); SoftPWMSetPercent(MotorL_IN2, 0);
  }

  if(degupdate == DegUpdateStep)
  {
    if(topdeg < topdeg_r) topdeg_r--;
    if(topdeg > topdeg_r) topdeg_r++;
    analogWrite(TopServoPin, degToPWM(topdeg_r));
    degupdate = 0;
  }
  else
  {
    degupdate++;
  }
}

void setup()
{
  Serial.begin(9600);
  mySerial.begin(9600);
  pinMode(LEDPIN, OUTPUT);
  
  pinMode(MotorR_IN1, OUTPUT);
  pinMode(MotorR_IN2, OUTPUT);
  pinMode(MotorL_IN1, OUTPUT);
  pinMode(MotorL_IN2, OUTPUT);
  
  pinMode(FRONT_DIST_PIN, INPUT);
  pinMode(BACK_DIST_PIN, INPUT);

  SoftPWMBegin();
  setPWMFadeTime(100, 50);

  pinMode(TopServoPin, OUTPUT);
  analogWrite(TopServoPin, degToPWM(topdeg_r));

  Serial.println("Init");
}

void loop()
{
  char c;
  if(mySerial.available())
  {
    c = mySerial.read();
    Serial.print(c);
    switch(c)
    {
      //大文字はモータ関係
      case 'F':
        mt = MT_FORWARD; break;
      case 'B':
        mt = MT_BACK; break;
      case 'R':
        mt = MT_RIGHT; break;
      case 'L':
        mt = MT_LEFT; break;
      case 'S':
        mt = MT_STOP; break;

      //小文字は首振り
      case 'u':
        topdeg -= SRVSTEP;
        if(topdeg < TopServoMin) topdeg = TopServoMin;
        break;
      case 'd':
        topdeg += SRVSTEP;
        if(TopServoMax < topdeg) topdeg = TopServoMax;
        break;
    }
  }

  control();
}
